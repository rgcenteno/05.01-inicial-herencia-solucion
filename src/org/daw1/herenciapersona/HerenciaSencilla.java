/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package org.daw1.herenciapersona;

import org.daw1.herenciapersona.personal.*;
/**
 *
 * @author Rafael González Centeno
 */
public class HerenciaSencilla {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Directivo d = new Directivo("1111111A", "Directivo A", CargoDirectivo.DIRECTOR);
        Alumno a = new Alumno("22222222B", "Estudiante B", Curso.CS1);
        Profesor p = new Profesor("33333333C", "Profesor C", "1CS");
        IES instituto = new IES("IES Pazo da Mercé");
        instituto.addPersona(p);
        instituto.addPersona(d);
        instituto.addPersona(a);
        System.out.println(instituto.showAllPeopleData());
    }
    
}
