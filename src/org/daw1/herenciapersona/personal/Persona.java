/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.daw1.herenciapersona.personal;

import com.google.common.base.Preconditions;
/**
 *
 * @author rgcenteno
 */
public class Persona implements Comparable<Persona>{
    private final String dni;
    private String nombre;
    
    public Persona(String nombre, String dni){
        checkDni(dni);
        this.nombre = nombre;
        this.dni = dni;
    }
    
    private static void checkDni(String dni){
        Preconditions.checkNotNull(dni);
        Preconditions.checkArgument(dni.matches("[0-9]{7,8}[A-Z]"), "El dni tiene que estar formado por 7 u 8 números y una letra");
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else{
            if(obj instanceof Persona){
                Persona aux = (Persona)obj;
                return this.dni.equals(aux.dni) && this.nombre.equals(aux.nombre);
            }
            else{
                return false;
            }
        }
    }
    
    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.nombre, this.dni);
    }

    @Override
    public int compareTo(Persona t) {
        return (this.nombre + this.dni).compareTo(t.nombre + t.dni);
    }

    @Override
    public String toString() {
        return this.nombre + " - " + this.dni;
    }
    
    
        
}
